const passport = require('passport');
const ExtractJWT = require('passport-jwt').ExtractJwt;
const JWTStrategy = require('passport-jwt').Strategy;

const config = require('../config/config');

const User = require('../models/User');

class passportManager {
	initialize() {
		var opts = {
			jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(), //ExtractJwt.fromAuthHeaderWithScheme('jwt'),
			secretOrKey: config.TOKEN_SECRET
		}

		passport.use(new JWTStrategy(opts, function(jwt_payload, done) {
			User.findOne({id: jwt_payload.id}, function(err, user) {
				if(err) {
					return done(err, false);
				}

				if(user) {
					done(null, user);
				} else {
					done(null, false);
				}
			});
		}));

		return passport.initialize();
	}

	authenticate(req, res, next){
		passport.authenticate('jwt', {session: false}, (err, user, info) => {
			console.log(user)
			console.log(info)
			if(err) { return next(err); }
			if(!user) {
				if(info.name == "TokenExpiredError") {
					return res.status(401).json({success:false, message: "El token ha expirado"});
				} else {
					return res.status(401).json({success:false, message: info.message });
				}
			}

			req.user = user;
			return next();
		})(req, res, next);
	};

	authenticates(req, res, next){
        passport.authenticate('jwt', { session: false}, (err, user, info) => {
          if (err) { return next(err); }
          if (!user) {
              if (info.name === "TokenExpiredError") {
                  return res.status(401).json({ message: "Your token has expired." });
              } else {
                  return res.status(401).json({ message: info.message });
              }
          }
          req.user = user;
          return next();
        })(req, res, next);
      };
}

module.exports = new passportManager();
