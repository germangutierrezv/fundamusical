const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const JWTStrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;
const config = require('../config/config');

const User = require('../models/User');

passport.use('local', new LocalStrategy({
	usernameField: 'user[email]',
	passwordField: 'user[password]'
}, async (email, password, done) => {
	const user = await User.findOne({email: email});
	if(!user || !user.matchPassword(password)) {
		return done(null, false, { message: 'Usuario o contraseña no valido.' });
	} 		

	return done(null, user, { message: 'Autenticación exitosamente'});
	
}));

passport.use('locales', new LocalStrategy({
	usernameField: 'user[email]',
	passwordField: 'user[password]'
}, async (email, password, done) => {
	const user = await User.findOne({email: email});
	if(!user) {
		return done(null, false, { message: 'Usuario no encontrado.' });
	} else {
		var match = user.matchPassword(password);
		if(match) {
			return done(null, user, { message: 'Autenticación exitosamente'});
		} else {
			return done(null, false, {message: 'Contraseña Incorrecta'});
		}
	}
}));

passport.use(new JWTStrategy({
	jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
	secretOrKey: config.TOKEN_SECRET,
}, async (payload, done) => {
	User.findOne({id: payload.id}, function(err, user) {
		if(err) {
			return done(err, false);
		}
		if(user) {
			return done(null, user);
		} else {
			return done(null, false);
		}
	});
}));

passport.serializeUser((user, done) => {
	done(null, user.id);
})

passport.deserializeUser((id, done) => {
	User.findById(id, (err, user) => {
		done(err, user);
	});
});
