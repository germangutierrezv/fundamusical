const path = require('path');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express');
const session = require('express-session');
//const passport = require('passport');

const passport = require('./helpers/passport');
require('./config/database');
const app = express();
const config = require('./config/config');

//setting
app.set('port', config.PORT);

//middleware
app.use(morgan('dev'));
app.use(cors());
app.use(bodyParser.json());
app.use(passport.initialize());
//app.use(passport.session());

//routes
app.use('/api/auth', require('./routes/auth'));
app.use('/api/users', require('./routes/users'));
app.use('/api/roles', require('./routes/roles'));
app.use('/api/permissions', require('./routes/permissions'));
app.use('/api/persons', require('./routes/persons'));
app.use('/api/students', require('./routes/students'));
app.use('/api/instruments', require('./routes/instruments'));
app.use('/api/professions', require('./routes/professions'));
app.use('/api/civilstatus', require('./routes/civilstatus'));

//static files
app.use(express.static(path.join(__dirname, 'public')));

//listing
app.listen(app.get('port'), () => {
	console.log('Server on port', app.get('port'));
});