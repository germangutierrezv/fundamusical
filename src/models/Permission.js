const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
	name: { type: String, required: true },
	description: { type: String },
	is_default: { type: Boolean, default: false },
	created: { type: Date, default: Date.now },
	updated: { type: Date }
});

module.exports = mongoose.model('Permission', schema);