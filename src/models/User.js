const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');

const User = new Schema({
	username: { type: String, unique:true },
	email: { type: String, required: true, unique: true },
	password: { type: String, required: true },
	created: { type: Date, default: Date.now },
	is_active: { type: Boolean, default: true },
	person: { 
		type: mongoose.Schema.Types.ObjectId,
      	ref: "Person"
    },
    role: { 
		type: mongoose.Schema.Types.ObjectId,
      	ref: "Role"
    }
});

User.methods.encryptPassword = async (password) => {
	const salt = await bcrypt.genSalt(10);
	const hash = bcrypt.hash(password, salt);
	return hash;
};

User.methods.comparePassword = function (password, cb) {
    bcrypt.compare(password, this.password, function (err, isMatch) {
        if (err) {
            return cb(err);
        }
        cb(null, isMatch);
    });
};

module.exports = mongoose.model('User', User);