const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
	name: { type: String, required: true },
	description: { type: String },
	created: { type: Date, default: Date.now }
});

module.exports = mongoose.model('State', schema);