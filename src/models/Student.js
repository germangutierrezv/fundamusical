const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
	first_name: { type: String, trim: true, required: [true, 'Primer nombre es requerido!'] },
	middle_name: { type: String },
	last_name: { type: String, required: [true, 'Primer apellido es requerido!'] },
	second_last_name: { type: String },
	image: { type: String },
	birth_date: { type: Date },
	identification: { type: String },
	nacionality: { type: String, lowercase: true },
	email: { type: String, lowercase: true},
	sex: { type: String, enum: ['M', 'F', 'O'],  required: [true, 'Genero es requerido!'] },
	laterality: { type: String, enum: ['D','Z','A'],  required: [true, 'Lateralidad es requerido!'] },
	birth_place: { type: String },
	observation: { type: String },
	height: { type: Number },
	weight: { type: Number },
	shirt_size: { type: Number },
	pant_size: { type: Number },
	shoe_size: { type: Number },
	status: { type: Boolean, default: true },
	created: { type: Date, default: Date.now },
	updated: { type: Date },
	civil_status: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'civilstatus',
		required: true
	},
	phone_number: { type: Boolean },
});


module.exports = mongoose.model('Student', schema);