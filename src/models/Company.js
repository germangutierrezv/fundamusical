const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Company = new Schema({
	name: { type: String, required: true },
	telephones: [{
		number: {
			type: String
		} 
	}],
	address: { type: String }
	created: { type: Date, default: Date.now }
});

module.exports = mongoose.model('companies', Company);