const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
	name: { type: String, required: [true, 'El nombre es obligatorio'], unique: true },
	description: { type: String },
	display_name: { type: String, required: [true, 'El nombre a mostrar es obligatorio'] },
	permission_roles: [{
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Role',
		required: true
	}],
	created: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Role', schema);