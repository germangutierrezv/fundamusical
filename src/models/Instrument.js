const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
	name: { type: String, required: true, unique:true},
	description: { type: String },
	code: { type: String },
	image: { type: String },
	created: { type: Date, default: Date.now },
	updated: { type: Date }
});

module.exports = mongoose.model('Instrument', schema);