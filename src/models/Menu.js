const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
	name: { type: String, required: true },
	icon: { type: String },
	order_index: { type: Number },
	has_children: { type: Boolean, default: false },
	permission: { 
		type: mongoose.Schema.Types.ObjectId,
      	ref: "Permission"
    },
    parent: { 
		type: mongoose.Schema.Types.ObjectId,
      	ref: "Menu"
    },
	created: { type: Date, default: Date.now },
	updated: { type: Date }
});

module.exports = mongoose.model('Menu', schema);