const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
	name: { type: String, required: true },
});

var CivilStatus;

schema.statics.list = function () {
  return CivilStatus.find()
}

module.exports = mongoose.model('CivilStatus', schema);