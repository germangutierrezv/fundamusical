const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
	first_name: { type: String, trim: true, required: [true, 'Primer nombre es requerido!'] },
	middle_name: { type: String },
	last_name: { type: String, required: [true, 'Primer apellido es requerido!']},
	second_last_name: { type: String },
	identification: { type: String, index: true, unique: true },
	email: { 
		type: String,
		index: true, 
		required: [true, 'Correo Electrónico es requerido!'],
		validate: {
             validator(email) {
                 return validator.isEmail(email);
             },
             message: '{VALUE} no es un correo electrónico valido!',
        }, 
		unique: true 
	},
	birth_date: { type: Date },
	sex: { type: String,  index: true, enum: ['M', 'F', 'O'], required: [true, 'Genero es requerido!'] },
	civil_status: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'civilstatus',
		required: [true, 'Estado civil es requerido!']
	},
	type_person: { 
		type: mongoose.Schema.Types.ObjectId, 
		ref: 'typepersons',
		required: true
	},
	profession: [{ 
		type: mongoose.Schema.Types.ObjectId, 
		ref: 'professions'
	}],
	created: { type: Date },
	updated: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Person', schema);