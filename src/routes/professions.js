const express = require('express');
const router = express.Router();
const passportManager = require('../helpers/passport');
const professionController = require('../controllers/professions');

router.route('/')
.get(passportManager.authenticate, professionController.all)
.post(passportManager.authenticate, professionController.add);

router.get('/:id', passportManager.authenticate, professionController.get);

router.put('/:id', passportManager.authenticate, professionController.edit);

module.exports = router;
