const express = require('express');
const router = express.Router();
const passportManager = require('../helpers/passport');
const permissionController = require('../controllers/permissions');

router.route('/')
.get(passportManager.authenticate, permissionController.all)
.post(passportManager.authenticate, permissionController.add);

router.get('/:id', passportManager.authenticate, permissionController.get);

router.put('/:id', passportManager.authenticate, permissionController.edit);

module.exports = router;
