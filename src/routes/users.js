const express = require('express');
const router = express.Router();
const passport = require('passport');
const passportManager = require('../helpers/passport');

const User = require('../models/User');
const helpers = require('../helpers/helper');

const userController = require('../controllers/users');

router.route('/')
.get(passportManager.authenticate, userController.all);

router.get('/profile', passportManager.authenticate, userController.profile);

module.exports = router