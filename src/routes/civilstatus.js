const express = require('express');
const router = express.Router();

const CivilStatus = require('../models/CivilStatus');

router.get('/', async (req, res) => {
	const civilstatus = await CivilStatus.find().sort({ name: 1 });
	res.json(civilstatus);
});

module.exports = router;