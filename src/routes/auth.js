const authController = require('../controllers/auth');
const express = require('express');
const router = express.Router();

router.post('/login', authController.signIn);

router.post('/logout', (req, res) => {
	res.send('sali')
});

module.exports = router;
