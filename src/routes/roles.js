const express = require('express');
const router = express.Router();
const passportManager = require('../helpers/passport');
const roleController = require('../controllers/roles');

router.route('/')
	.get(passportManager.authenticate, roleController.all)
	.post(passportManager.authenticate, roleController.add);

router.get('/:id', passportManager.authenticate, roleController.get);

router.put('/:id', passportManager.authenticate, roleController.edit);

router.route('/:id/permissions')
	.post(passportManager.authenticate, roleController.add_permissions)
	.get(passportManager.authenticate, roleController.get_permissions)
	.put(passportManager.authenticate, roleController.edit_permissions);


module.exports = router;
