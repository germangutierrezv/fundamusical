const express = require('express');
const router = express.Router();
const passportManager = require('../helpers/passport');
const instrumentController = require('../controllers/instruments');

const Instrument = require('../models/Instrument')
const TypeInstrument = require('../models/TypeInstrument')

router.route('/')
.get(passportManager.authenticate, instrumentController.all)
.post(passportManager.authenticate, instrumentController.add);

router.get('/:id', passportManager.authenticate, instrumentController.get);

router.put('/:id', passportManager.authenticate, instrumentController.edit);

router.delete('/:id', passportManager.authenticate, instrumentController.delete);

router.get('/types/all', passportManager.authenticate, instrumentController.all_type);

router.post('/types', passportManager.authenticate, instrumentController.add_type);

router.get('/types/:id', passportManager.authenticate, instrumentController.get_type);

router.put('/types/:id', passportManager.authenticate, instrumentController.edit_type);

module.exports = router;
