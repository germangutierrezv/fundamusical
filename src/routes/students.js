const express = require('express');
const router = express.Router();
const passportManager = require('../helpers/passport');
const studentController = require('../controllers/students');


const Student = require('../models/Student')


router.route('/')
.get(passportManager.authenticate, studentController.all)
.post(passportManager.authenticate, studentController.add);

router.get('/:id', passportManager.authenticate, studentController.get);

router.put('/:id', passportManager.authenticate, studentController.edit);


//Pendiente validar
router.delete('/:id', async (req, res) => {
	const student = await Student.findByIdAndRemove(req.params.id);
	if(student) {
		return res.status(200).json({
			status: 'Estudiante eliminado'
		});
	} else {
		return res.status(422).json({
			status: 'Estudiante no encontrado'
		});
	}
});
module.exports = router;
