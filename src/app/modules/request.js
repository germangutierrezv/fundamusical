import axios from 'axios'
import * as Config from '../config/index'

export function get(route) {
	return axios.get(Config.apiPath + route)
}
export function post(route, data) {
	return axios.post(Config.apiPath + route, data)
}
export function put(route, data) {
	return axios.put(Config.apiPath + route, data)
}
export function del(route) {
	return axios.delete(Config.apiPath + route)
}
