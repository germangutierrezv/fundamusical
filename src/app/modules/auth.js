export default function(Vue) {
	Vue.auth = {
		getToken() {
			var token = localStorage.getItem('token');

			if(!token) {
				return null;
			}
			
			if(token == undefined) {
				this.destroyToken();
				return null;
			}
			
			return token;
		},
		authenticated() {
			if(this.getToken()) {
				return true;
			} else {
				return false;
			}
		}
 	}

	Object.defineProperties(Vue.prototype,{
		$auth: {
			get() {
				return Vue.auth
			}
		}

	})
}
