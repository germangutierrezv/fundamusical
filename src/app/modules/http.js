import { get, post, put, del } from './request'

export default function(Vue) {
	Vue.http = {
		login(data) {
			return post('auth/login', data)
		},
		logout() {
			return post('logout')
		},
 		civil_status() {
 			return get('civilstatus')
 		},

 		//Roles
 		roles() {
 			return get('roles')
		},
		role(id) {
			return get('roles/' + id)
		},
		add_roles(data) {
			return post('roles', data)
		},
		edit_role(id, data) {
			return put('roles/' + id, data)
		},

		//Permisos
		permissions() {
			return get('permissions')
		},
		add_permission(data) {
			return post('permissions', data)
		},

 		//Usuarios
		users() {
 			return get('users')
		},
		//Estudiantes
 		students() {
 			return get('students')
 		},
 		add_students(data) {
 			return post('students', data)
 		},
 		student(id) {
 			return get('students/' + id)
 		},
 		//Tipos de Instrumentos
 		types_instruments() {
 			return get('instruments/types/all')
 		},
 		type_instrument(id) {
 			return get('instruments/types/' + id)
 		},
 		add_type_instrument(data) {
 			return post('instruments/types', data)
 		},
 		edit_type_instrument(id, data) {
 			return put('instruments/types/' + id, data)
 		},
 		//Instrumentos
 		instruments() {
 			return get('instruments')
 		},
 		create_instruments(data) {
 			return post('instruments', data)
 		},
 		del_instruments(id) {
 			return del('instruments/' + id)
 		}
 	}

	Object.defineProperties(Vue.prototype,{
		$http: {
			get() {
				return Vue.http
			}
		}

	})
}
