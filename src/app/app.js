import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './views/App.vue';
import router from './routes/index';
import './mixins/mixins';
import {store} from './store';
import Http from './modules/http';
import Auth from './modules/auth';
import './routes/interceptors';

import BootstrapVue from 'bootstrap-vue';

Vue.use(BootstrapVue);
Vue.use(Http);
Vue.use(Auth);

import '../../node_modules/bootstrap/dist/css/bootstrap.css';
import '../../node_modules/bootstrap-vue/dist/bootstrap-vue.css';


Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  store,
  router,
  components: { App }
}).$mount('#app')
