import Vue from 'vue'
import moment from 'moment'
moment.locale('es');

Vue.mixin({
    data() {
      return {
        paginate_config: {
          perPage: 10,
          pageOptions: [5, 10, 20, 50]
        }
      }
    },
    methods: {
      formatDateNow(val,range) {
        var m = moment();
        var date  
        if (val > 0) {
          date = m.add(val, range).format('YYYY-MM-DD')
        } else if(val < 0) {
          date = m.subtract(Math.abs(val), range).format('YYYY-MM-DD')
        } else {
          date = m
        }
        return date
      },
      formatDate (date) {
      	if(date != null) {
        	return moment(date).format('DD-MM-YYYY'); //new Date(date).toISOString().substr(0, 10)
      	} else {
      		return " - ";
      	}
      },
      formatDateStand (date) {
        if(date != null) {
          return moment(date).format('YYYY-MM-DD'); 
        } else {
          return " - ";
        }
      },
      formatDateHour (date) {
        if(date != null) {
          return moment(date).format('DD-MM-YYYY HH:mm'); 
        } else {
          return "";
        }
      },
      formatDateDMYH (date) {
        if(date != null) {
          return moment(date).format('DD [de] MMMM [de] YYYY HH:mm');
        } else {
          return " - ";
        }
      },
      formatDateHourSeg (date) {
        if(date != null) {
          return moment(date).format('DD-MM-YYYY HH:mm:ss'); //new Date(date).toISOString().substr(0, 10)
        } else {
          return "";
        }
      },
      calculateAge(date) {
        console.log(date)
        if(date) {
          var m = moment(date, "YYYY-MM-DD")
          if(m.isValid()) {
            return m.fromNow(true)
          } 
        } else {
          return ''
        }

      },
      initialString (string) {
        if(string) {
          let ini = string.substr(0,1);
          ini = ini.toUpperCase();
          
          return ini;
        }
      },
      formatUpperPrimary(string) {
        if(string) {
          return string.charAt(0).toUpperCase() + string.slice(1)
        }
      },
      formatUpper (string) {
        if(string) {
          return string.toUpperCase();
        }
      },
      formatInteger (number) {
        if(number) {
          return parseInt(number);
        }
      },
      formatCard(string) {
        if(string) {
          let long = string.length;
          let posFin = long - 5;
          
          let array = [];

          array = string.split("");

          for(let i=5; i<posFin; i++) {
            array[i] = 'X';
          }

          string = array.join("");

          return string;
        }
      },
      formatAddress(string) {
        if(string) {
          let add = string.substr(0,50);

          return add + '...';
        }
      },
      validEmail(email) {
        var reg = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        var regOficial = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

        if (reg.test(email) && regOficial.test(email)){
         return true
        } else {
         return false
        }
      },
      encrypt(value) {
        if(value.length > 0) {
          return window.btoa(value);
        } 
      },
      desencrypt(value) {
        if(value.length > 0) {
          return window.atob(valor);
        }
      },
    }
  })
  