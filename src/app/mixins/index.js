export const config = {
  data() {
    return {
      paginate_config: {
        perPage: 10,
        pageOptions: [5, 10, 20, 50]
      }
    };
  }
};