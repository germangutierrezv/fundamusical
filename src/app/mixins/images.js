export const Images = {
	data() {
	    return { 
	      base: window.location.origin,
	      route_images: ''
	    }
	},
	computed: {
	    routeImage() {
	      return this.img()
	    },
	    routeImagePersons() {
	      return this.img('images') 
	    }
	},
	methods: {
       	img(route) {
	    	if(route) {
	        	return this.route_images + route + '/'
	    	} else {
	        	return this.route_images
	      	}
	    }
	},
	created() {
	    this.route_images = this.base + '/img/'
	}
}