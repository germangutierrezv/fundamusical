import axios from 'axios'
import * as Config from '../../config'

const getDefaultState = () => {
  return {
    user: null,
    authenticated: false,
    token: null
  }
}

const state = getDefaultState()

const mutations = {
  'SAVE_TOKEN' (state, data) {
    localStorage.setItem('token', data.token)
    state.token = data.token
    state.authenticated = true
  },

  'SET_TOKEN' (state, data) {
    state.token = data
    state.authenticated = true
  },

  SET_CURRENT_USER(state, data) {
    state.user = data
  },

  LOGOUT(state) {
    localStorage.removeItem('token')
    Object.assign(state, getDefaultState())
  }
}

const actions = {

  saveToken({commit}, token) {
    commit('SAVE_TOKEN', token)
  },

  setToken({commit}, token) {
    commit('SET_TOKEN', token)
  },

  setCurrentUser({commit}, user) {
    commit('SET_CURRENT_USER', user)
  },

  logout({commit}) {
    
        commit('LOGOUT')

    /*return new Promise((resolve, reject) => {
      axios.post('/api/logout')
      .then(function (response) {
        commit('LOGOUT')
        resolve()
      })
      .catch(function (error) {
        console.log(error)
        reject(error)
      });
    }, error => console.log(error))*/
  },

  refreshToken({commit}) {
    return new Promise((resolve, reject) => {
      axios.post('/api/refresh-token')
      .then(function (response) {
        resolve(response)
      })
      .catch(function (error) {
        console.log(error)
        reject(error)
      });
    }, error => console.log(error))
  }
}

const getters = {
  currentUser: (state) => {
    return state.user
  },

  authenticated: (state) => {
    return state.authenticated
  },

  token: (state) => {
    return state.token
  },
}

export default {
  state,
  mutations,
  actions,
  getters,
}

