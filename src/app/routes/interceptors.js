import Vue from 'vue';
import axios from 'axios';
import store from '../store';

/**
 * Request
 */
axios.interceptors.request.use(
  (config) => {
    var token = localStorage.getItem('token');

    if(token) {
      config.headers['Authorization'] = 'Bearer ' + token
    }

    config.headers['Accept'] = 'application/json'
    config.headers['Accept-Language'] = 'es'

    return config
  },
  (error) => {
    return Promise.reject(error)
  }
)

/**
 * Response
 */
axios.interceptors.response.use(
  (response) => {
    console.log(response)
    return response
  },
  (error) => {
    const originalRequest = error.config
console.log(error.response)
console.log(store)
    // token expired
    if (error.response.status === 401 && error.response.statusText === 'Unauthorized') {
      var message = "Espera mientras se redirecciona a la página de inicio de sesión"
      
      /*this.$bvToast.toast(message, {
        title: 'Debe iniciar sesión',
        variant: 'danger',
        'auto-hide-delay': 1000,
        solid: true
      });
*/
      setTimeout(function(){
        console.log(store)
        //Vue.auth.destroyToken();
        this.$store.dispatch('logout')
        window.location.href = '/login';
      },80);

      /*originalRequest._retry = true

      store.dispatch('refreshToken').then((response) => {
        // console.log(response)
        let token = response.data.token
        let headerAuth = 'Bearer ' + response.data.token

        store.dispatch('saveToken', token)

        axios.defaults.headers['Authorization'] = headerAuth
        originalRequest.headers['Authorization'] = headerAuth

        return axios(originalRequest)
      }).catch((error) => {
        store.dispatch('logout').then(() => {
          router.push({ name: 'login' })
        }).catch(() => {
          router.push({ name: 'login' })
        })
      })*/
    }

    return Promise.reject(error)
  }
)