function getValidToken() {
    const token = localStorage.token 
    if (token && token.length > 0 && token != undefined ) {
        return true;
    } else {
        return false;
    }    
} 
function loggedInRedirectDashboard(to, from, next) {
    console.log(to)
    if (getValidToken()) {
        next('/admin');
    } else {
        next();
    }
}

function isLoggedIn(to, from, next) {
    if (getValidToken()) {
        next();
    } else {
        next('/login');
    }
}

import AppNotFound from '../components/AppNotFound';
import AppAdmin from '../views/layout/AppAdmin';
import AppLogin from '../views/layout/AppLogin';
import Login from '../views/login/Login';
import Dashboard from '../views/admin/Dashboard';
import AppIndexUsers from '../views/admin/users/AppIndex';
import AppIndexRoles from '../views/admin/roles/AppIndex';
import AppIndexPermissions from '../views/admin/permissions/AppIndex';
import AppIndexPersons from '../views/admin/persons/AppIndex';
import AppIndexStudents from '../views/admin/students/AppIndex';
import AppCreateStudents from '../views/admin/students/AppCreate';
import AppEditStudents from '../views/admin/students/AppEdit';
import AppIndexTypesInstruments from '../views/admin/typesinstruments/AppIndex';
import AppIndexInstruments from '../views/admin/instruments/AppIndex';

export default [
  //Account
    { path: '/', component: AppLogin,
        children: [
            { path: '', component: Login, beforeEnter:loggedInRedirectDashboard },
            { path: 'login', component: Login, beforeEnter:loggedInRedirectDashboard },
            //{ path: 'login', component: () => import('../views/login/Login.vue'), beforeEnter:loggedInRedirectDashboard },
            /*{ path: 'logout', 
                beforeEnter (to: any, from: any, next: any) {
                    //do logout logic
                    next('/');
                } 
            },*/
            //{ path: 'register', component: () => import('./components/account/register.vue') }
        ]
    },

    //Admin
    { path: '/admin', component: AppAdmin,
        children: [
            { path: '', component: Dashboard, beforeEnter: isLoggedIn },
            { path: 'dashboard', component: Dashboard, name: 'dashboard', beforeEnter: isLoggedIn },
            { path: 'roles', component: AppIndexRoles, beforeEnter: isLoggedIn },
            { path: 'permissions', component: AppIndexPermissions, beforeEnter: isLoggedIn },
            { path: 'users', component: AppIndexUsers, beforeEnter: isLoggedIn },
            { path: 'students', component: AppIndexStudents, beforeEnter: isLoggedIn },
            { path: 'students/new', component: AppCreateStudents, beforeEnter: isLoggedIn },
            { path: 'students/edit', name:'students_edit', props: true, component: AppEditStudents, beforeEnter: isLoggedIn },
            { path: 'students/view', name:'students_view', props:true, component: AppEditStudents, beforeEnter: isLoggedIn },
            { path: 'instruments', component: AppIndexInstruments, beforeEnter: isLoggedIn },
            { path: 'typesinstruments', component: AppIndexTypesInstruments, beforeEnter: isLoggedIn },
            
            { path: 'persons', component: AppIndexPersons },
            { path: '*', component: AppNotFound }    
        ]    
    }     
]