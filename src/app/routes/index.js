import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './routes';

Vue.use(VueRouter);

const router = new VueRouter({
    //mode: "history",
    routes,
    linkActiveClass: 'active',
})

/*router.beforeEach((to, from, next) => {
    console.log(to.fullPath)
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!Vue.$auth.authenticated) {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else if (to.matched.some(record => record.meta.redirectIfLogged)) {
    if (Vue.$auth.authenticated) {
      next({
        path: '/'
      })
    } else {
      next()
    }
  } else {
    next()
  }
})
*/
export default router