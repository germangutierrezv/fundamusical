export const Model = {
	methods: {
		init() {
			this.options_sex= [
                { value: 'F', text: 'Femenino' },
                { value: 'M', text: 'Masculino' },
                { value: 'O', text: 'Otro' }
            ]

            this.options_lat= [
                { value: 'D', text: 'Diestro' },
                { value: 'Z', text: 'Zurdo' },
                { value: 'A', text: 'Ambos' }
            ]

            this.options_status= [
                { value: true, text: 'Activo' },
                { value: false, text: 'Inactivo' },
            ]

            this.civil_status()
		},
		civil_status() {
			this.$http.civil_status()
            .then(response => {
                var options = response.data
                for(let i in options) {
                    let row = {}
                    row.value = options[i]._id
                    row.text = options[i].name

                    this.options_civil.push(row)
                }
            })
		}
	}

}