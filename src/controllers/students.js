const Student = require('../models/Student');

class Students {
	async all(req, res) {
		const students = await Student.find({}, "first_name last_name identification sex birth_date");
		res.json(students);
	}

	async get(req, res) {
		if(!req.params.id) {
			return res.status(422).json({
				status: 'ID requerido'
			});
		}
		
		const student = await Student.findOne({_id: req.params.id});
		res.json(student);
	}

	async add(req, res) {
		const errors = validation(req, res);

		if(errors && errors.length > 0) {
			return res.status(422).json(errors);
		}

		const student = new Student(req.body);
		await student.save();
		
		return res.status(201).json({
			status: 'Estudiante guardado'
		});
	}

	async edit(req, res) {
		if(!req.params.id) {
			return res.status(422).json({
				status: 'ID requerido'
			});
		}

		const errors = validation(req, res);

		if(errors && errors.length > 0) {
			return res.status(422).json(errors);
		}

		const student = await Student.findByIdAndUpdate(req.params.id, req.body);
		
		if(student) {
			return res.status(200).json({
				status: 'Estudiante actualizado'
			});	
		} else {
			return res.status(422).json({
				status: 'Estudiante no encontrado'
			});
		}

	}

}

function validation(req,res) {
	const fields= {
		first_name: "Es requerido el primer nombre", 
		last_name: "Es requerido el primer apellido",
		sex: "Es requerido el sexo",
		laterality: "Es requerido la lateralidad",
		civil_status: "Es requerido el estado civil"
	}

	const errors = [];

	if(Object.keys(req.body).length == 0) {
		for(let i in fields) {
			errors.push({[i]: fields[i]})
		}
		return errors
	}

	for(let j in fields) {
		if(req.body[j] == undefined) {
			errors.push({[j]: fields[j]})
		}
	}

	if(errors.length > 0) { return errors }
}

module.exports = new Students();