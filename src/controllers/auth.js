const jwt = require('jsonwebtoken');
const config = require('../config/config');

const User = require('../models/User');

class Auth {
	async signUp(req, res) {

	}

	async signIn(req, res) {
		const email = req.body.email;
		const password = req.body.password; 

		const fields= {
			email: "Es requerido el correo electrónico", 
			password: "Es requerido la contraseña"
		}

		const errors = [];

		if(Object.keys(req.body).length == 0) {
			for(let i in fields) {
				errors.push({[i]: fields[i]})
			}
			return res.status(422).json(errors);
		}

		for(let j in fields) {
			if(req.body[j] == undefined) {
				errors.push({[j]: fields[j]})
			}
		}

		if(errors.length > 0) { return res.status(422).json(errors); }

		const user = await User.findOne({ email });

		if(!user) {
			return res.status(422).json({sucess: false, message: 'Correo electrónico no encontrado'});
		}

		user.comparePassword(password, (err, isMatch) => {
			if (isMatch && !err) {
	            var token = jwt.sign(user.toJSON(), config.TOKEN_SECRET,{ expiresIn: '6000m' });
	            res.json({success: true, token: token});
	          } else {
	            res.status(401).send({success: false, message: 'Autenticación fallida.'});
	          }			
		});
	}
}

module.exports = new Auth();