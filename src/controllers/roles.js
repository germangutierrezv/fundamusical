const Role = require('../models/Role');

class Roles {
	async all(req, res) {
		const roles = await Role.find();
		res.json(roles);
	}

	async get(req, res) {
		if(!req.params.id) {
			return res.status(422).json({
				status: 'ID requerido'
			});
		}

		const role = await Role.findOne({_id: req.params.id});
		res.json(role);
	}

	async add(req, res) {
		const errors = validation(req, res);

		if(errors && errors.length > 0) {
			return res.status(422).json(errors);
		}

		const role = new Role(req.body);
		try {
			await role.save()
			return res.status(201).json({
				status: 'Rol guardado'
			});
		} catch(e) {
			console.log(e)
			res.status(500).jsonp({
		      success: false,
		      errors: e.errmsg
		    });

		}

	}

	async edit(req, res) {
		const errors = validation(req, res);

		if(!req.params.id) {
			return res.status(422).json({
				status: 'ID requerido'
			});
		}

		if(errors && errors.length > 0) {
			return res.status(422).json(errors);
		}

		const role = await Role.findByIdAndUpdate(req.params.id, req.body);
		if(!role) {
			return res.status(422).json({
				status: 'Rol no encontrado'
			});
		} 

		return res.status(200).json({
			status: 'Rol actualizado'
		});	
	}

	async add_permissions(req, res) {

	}

	async edit_permissions(req, res) {

	}
	
	async get_permissions(req, res) {
		if(!req.params.id) {
			return res.status(422).json({
				status: 'ID requerido'
			});
		}
	}


}


function validation(req,res) {
	const errors = [];
	const { name } = req.body;

	if(!name) {
		errors.push({name: 'Es requerido el nombre'});
	}

	if(errors.length > 0) {
		return errors
	}
}

module.exports = new Roles();
