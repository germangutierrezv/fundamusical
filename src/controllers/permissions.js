const Permission = require('../models/Permission');

class Permissions {
	async all(req, res) {
		const permissions = await Permission.find();
		res.json(permissions);
	}

	async get(req, res) {
		if(!req.params.id) {
			return res.status(422).json({
				status: 'ID requerido'
			});
		}
		
		const permission = await Permission.find({_id: req.params.id});
		res.json(permission);
	}

	async add(req, res) {
		const errors = validation(req, res);

		if(errors && errors.length > 0) {
			return res.status(422).json(errors);
		}

		const permission = new Permission(req.body);
		await permission.save();
		return res.status(201).json({
			status: 'Permiso guardado'
		});
	}

	async edit(req, res) {
		const errors = validation(req, res);

		if(errors && errors.length > 0) {
			return res.status(422).json(errors);
		}

		const permission = await Permission.findByIdAndUpdate(req.params.id, req.body);
		if(!permission) {
			return res.status(422).json({
				status: 'Permiso no encontrado'
			});
		} 

		return res.status(200).json({
			status: 'Permiso actualizado'
		});	
	}

}


function validation(req,res) {
	const errors = [];
	const { name } = req.body;

	if(!name) {
		errors.push({name: 'Es requerido el nombre'});
	}

	if(errors.length > 0) {
		return errors
	}
}

module.exports = new Permissions();
