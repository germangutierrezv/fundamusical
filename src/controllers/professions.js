const Profession = require('../models/Profession');

class Professions {
	async all(req, res) {
		const professions = await Profession.find();
		res.json(professions);
	}

	async get(req, res) {
		if(!req.params.id) {
			return res.status(422).json({
				status: 'ID requerido'
			});
		}

		const profession = await Profession.find({_id: req.params.id});
		res.json(profession);
	}

	async add(req, res) {
		const errors = validation(req, res);

		if(errors && errors.length > 0) {
			return res.status(422).json(errors);
		}

		const profession = new Profession(req.body);
		await profession.save();
		return res.status(201).json({
			status: 'Profesión guardada'
		});
	}

	async edit(req, res) {
		if(!req.params.id) {
			return res.status(422).json({
				status: 'ID requerido'
			});
		}
		
		const errors = validation(req, res);

		if(errors && errors.length > 0) {
			return res.status(422).json(errors);
		}

		const profession = await Profession.findByIdAndUpdate(req.params.id, req.body);
		if(!profession) {
			return res.status(422).json({
				status: 'Profesión no encontrada'
			});
		} 

		return res.status(200).json({
			status: 'Profesión actualizada'
		});	
	}

}


function validation(req,res) {
	const errors = [];
	const { name } = req.body;

	if(!name) {
		errors.push({name: 'Es requerido el nombre'});
	}

	if(errors.length > 0) {
		return errors
	}
}

module.exports = new Professions();
