const User = require('../models/User');

class Users {
	profile(req, res) {
		if(req.user) {
		    return res.json({ user: req.user });
		}
		const { payload: { id } } = req;

		return Users.findById(id)
		    .then((user) => {
		      if(!user) {
		        return res.sendStatus(400);
		      }

		      return res.json({ user: user });
		    });
	}

	async all(req, res) {
		const users = await User.find();
		res.json(users);
	}
}

module.exports = new Users();
