const Instrument = require('../models/Instrument');
const TypeInstrument = require('../models/TypeInstrument');

class Instruments {
	async all(req, res) {
		const instruments = await Instrument.find();
		res.json(instruments);
	}

	async get(req, res) {
		if(!req.params.id) {
			return res.status(422).json({
				status: 'ID requerido'
			});
		}
		
		const instrument = await Instrument.find({_id: req.params.id});
		res.json(instrument);
	}

	async add(req, res) {
		const errors = validation(req, res);

		if(errors && errors.length > 0) {
			return res.status(422).json(errors);
		}

		const instrument = new Instrument(req.body);
		await instrument.save();
		return res.status(201).json({
			status: 'Instrumento guardado'
		});
	}

	async edit(req, res) {
		const errors = validation(req, res);

		if(errors && errors.length > 0) {
			return res.status(422).json(errors);
		}

		const instrument = await Instrument.findByIdAndUpdate(req.params.id, req.body);
		if(instrument) {
			return res.status(200).json({
				status: 'Instrumento actualizado'
			});	
		} else {
			return res.status(422).json({
				status: 'Instrumento no encontrado'
			});
		}
	}

	async delete(req, res) {
		const instrument = await Instrument.findByIdAndRemove(req.params.id);
		
		if(instrument) {
			res.status(200).json({
				status: 'Instrumento eliminado'
			});
		} else {
			res.status(422).json({
				status: 'Instrumento no encontrado'
			});
		}
	}

	async all_type(req, res) {
		const type_instruments = await TypeInstrument.find();
		res.json(type_instruments);
	}

	async get_type(req, res) {
		const type_instrument = await TypeInstrument.findOne({_id: req.params.id});
		res.json(type_instrument);
	}

	async add_type(req, res) {
		const errors = validation_type(req, res);

		if(errors && errors.length > 0) {
			return res.status(422).json(errors);
		}

		const typeinstrument = new TypeInstrument(req.body);
		if(await typeinstrument.save()) {
			return res.status(201).json({
				status: 'Tipo de instrumento guardado'
			});
		} else {
			return res.status(422).json('Error al guardar');
		}
	}

	async edit_type(req, res) {
		const errors = validation_type(req, res);

		if(errors && errors.length > 0) {
			return res.status(422).json(errors);
		}

		const typeinstrument = await TypeInstrument.findByIdAndUpdate(req.params.id, req.body);

		if(typeinstrument) {
			return res.status(200).json({
				status: 'Tipo de instrumento actualizado'
			});	
		} else {
			return res.status(422).json({
				status: 'Tipo de instrumento no encontrado'
			});
		}
	}
} 

function validation(req,res) {
	const fields= {
		first_name: "Es requerido el primer nombre", 
		last_name: "Es requerido el primer apellido",
		sex: "Es requerido el sexo",
		laterality: "Es requerido la lateralidad",
		civil_status: "Es requerido el estado civil"
	}

	const errors = [];

	if(Object.keys(req.body).length == 0) {
		for(let i in fields) {
			errors.push({[i]: fields[i]})
		}
		return errors
	}

	for(let j in fields) {
		if(req.body[j] == undefined) {
			errors.push({[j]: fields[j]})
		}
	}

	if(errors.length > 0) { return errors }
}

function validation_type(req,res) {
	const errors = [];
	const { name } = req.body;

	if(!name) {
		errors.push({name: 'Es requerido el nombre'});
	}

	if(errors.length > 0) {
		return errors
	}
}

module.exports = new Instruments();