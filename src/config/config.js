module.exports = {
	PORT: process.env.PORT || 3000,
	MONGO_URI: process.env.MONGO_URI || 'mongodb://localhost/fundamusical',
	TOKEN_SECRET: process.env.TOKEN_SECRET || "tokenultrasecreto",
	JWT_SECRET: process.env.JWT_STRATEGY_SECRET || "jwtsecreto",
};
