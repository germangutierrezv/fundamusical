const mongoose = require('mongoose');
const config = require('./config');
const mongoURI = config.MONGO_URI;

mongoose.Promise = global.Promise;

mongoose.connect(mongoURI, {
	useNewUrlParser: true,
	useFindAndModify: false,
	useCreateIndex: true
})
.then(() => { console.log('DB is connected') })
.catch(err => console.error(err));