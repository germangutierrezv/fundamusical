<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(PersonaTableSeeder::class);
    }
}

/**
* 
*/
class AlumnoTableSeeder extends Seeder
{
	public function run()
	{
		App\Alumno::truncate();

        $faker= Faker::create();
        for($j=1; $j<=50; $j++)
        {
            $p1=new App\Alumno();
            $p1->expediente="EXP-".$j; 
            $p1->nombres=$faker->name;
            $p1->apellidos=$faker->name;
            $p1->email=$faker->name.$j.'@'.$faker->firstNameFemale.'.com';
            $p1->cedula="V".str_random(4,7);
            $p1->direccion=join("\n\n", $faker->paragraphs(mt_rand(3, 6)));
            $p1->user_id=1;
            $p1->estatus=$faker->randomElement(['S','I','A']);
            $p1->sexo=$faker->randomElement(['O','F','M']);
            $p1->f_nacimiento= $faker->dateTimeBetween('-48 month', '+60 days');
            $p1->estado=$faker->randomElement(['MDA','BRS','TRU']);
            $p1->estado_civil=$faker->randomElement(['S','C','V','D']);
            $p1->f_ingreso= $faker->dateTimeBetween('-1 month', '+15 days');
            $p1->estudia=$faker->randomElement(['0','1']);
            

            $p1->save();
            }
        } 
	
}

class PersonaTableSeeder extends Seeder
{
    public function run()
    {
        //App\Persona::truncate();

        $faker= Faker::create();
        for($j=1; $j<=50; $j++)
        {
            $p1=new App\Persona(); 
            $p1->nombres=$faker->name;
            $p1->apellidos=$faker->name;
            $p1->email=$faker->name.$j.'@'.$faker->firstNameFemale.'.com';
            $p1->cedula="V".str_random(4,7);
            $p1->direccion=join("\n\n", $faker->paragraphs(mt_rand(3, 6)));
            $p1->user_id=1;
            $p1->sexo=$faker->randomElement(['O','F','M']);
            $p1->f_nacimiento= $faker->dateTimeBetween('-48 month', '+60 days');
            $p1->estado=$faker->randomElement(['MDA','BRS','TRU']);
            $p1->estado_civil=$faker->randomElement(['S','C','V','D']);
            $p1->isprofesor=$faker->randomElement(['0','1']);
            $p1->estatus=$faker->randomElement(['S','I','A']);

            $p1->f_ingreso= $faker->dateTimeBetween('-89 month', '+105 days');
            

            $p1->save();
            }
        } 
    
}
