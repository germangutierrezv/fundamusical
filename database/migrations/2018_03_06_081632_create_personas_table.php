<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombres');
            $table->string('apellidos');
            $table->string('sexo',1);
            $table->string('cedula')->unique();
            $table->string('foto')->default('no_foto.png');
            $table->datetime('f_nacimiento')->index();
            $table->string('email')->unique();
            $table->string('estado_civil',1);
            $table->text('direccion');
            $table->string('estado');
            $table->string('telefono')->nullable();
            $table->integer('user_id');
            $table->boolean('isprofesor');
            $table->datetime('f_ingreso');
            $table->string('estatus',1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}
