<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlumnosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alumnos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('expediente');
            $table->string('nombres');
            $table->string('apellidos');
            $table->string('sexo',1);
            $table->string('cedula')->unique()->nullable();
            $table->string('foto')->default('no_foto.png');
            $table->datetime('f_nacimiento')->index();
            $table->string('email')->unique()->nullable();
            $table->string('estatus',1);
            $table->string('estado_civil',1);
            $table->datetime('f_ingreso')->nullable();
            $table->text('direccion');
            $table->string('estado');
            $table->string('telefono')->nullable();
            $table->integer('user_id');
            $table->boolean('estudia');
            $table->string('l_estudia')->nullable();
            $table->string('nivel')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alumnos');
    }
}
