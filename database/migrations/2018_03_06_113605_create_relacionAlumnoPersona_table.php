<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelacionAlumnoPersonaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rel_AlumnoPersona', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('alumno_id')->unsigned();
            $table->foreign('alumno_id')
                    ->references('id')->on('alumnos')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->integer('persona_id')->unsigned();
            $table->foreign('persona_id')
                    ->references('id')->on('personas')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->string('relacion',3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rel_AlumnoPersona');
    }
}
