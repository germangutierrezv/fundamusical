<?php 
	//Formato de fecha
	function getFormatDate($date){
		return date('Y-m-d',strtotime($date));
	}

    function getFormatDateComplete($date){
        return date('Y-m-d h',strtotime($date));
    }

    function getFormatFecha($date){
        return date('d-m-Y',strtotime($date));
    }

	//Estatus del alumno
	function getEstatus($est_al) {
        switch ($est_al) {
        	case 'A':
        		$estatus = 'Activo';
        		break;
        	case 'I':
        		$estatus = 'Inactivo';
        		break;
        	case 'S':
        		$estatus = 'Suspendido';
        		break;
        }

        return $estatus;
    } 

    function getSexo($sexo)
    {
    	switch ($sexo) {
    		case 'M':
    			$sexo = 'Masculino';
    			break;
    		case 'F':
    			$sexo = 'Femenino';
    			break;    		
    		case 'O':
    			$sexo = 'Otro';
    			break;
    	}

    	return $sexo;
    }

    //Devuelve la ruta de la foto
    function getImage($table, $id)
    {   
        $foto = DB::table($table)->where('id', $id)->pluck('foto')->implode(' ');
        if($foto == 'no_foto.png'){
            return asset('img/no_foto.png');
        } else {
            return asset('img/'). $table .'/'. $foto;
        }
        
    }

    function getEdad($f_nac) 
    {
        $f_desde = new DateTime($f_nac);
        $f_hasta = new DateTime('today');
        $edad = $f_desde->diff($f_hasta)->y;
        return $edad;
    }

    function estatusAlumno() {
        $estados = array();
        $estados['A'] = 'Activo';
        $estados['I'] = 'Inactivo';
        $estados['S'] = 'Suspendido';
	
	    return $estados;
    }

    function estadoCivil() {
        $estadoC = array();
        $estadoC['S'] = 'Soltero';
        $estadoC['C'] = 'Casado';
        $estadoC['V'] = 'Viudo';
        $estadoC['B'] = 'Concubino';
        $estadoC['D'] = 'Divorciado';
	
	    return $estadoC;
    }

    function estadoPais() {
        $estado = array();
        $estado['MDA'] = 'Mérida';
        $estado['BRS'] = 'Barinas';
        $estado['ZLA'] = 'Zulia';
        $estado['TRU'] = 'Trujillo';
        $estado['TAC'] = 'Tachira';
        $estado['LAR'] = 'Lara';
        $estado['FAL'] = 'Falcón';
	
	    return $estado;
    }

    function getEstadoPais($key){
        $estados = estadoPais();
        return $estados[$key];
    }
    
    function getEstadoCivil($key){
        $estado_civil = estadoCivil();
        return $estado_civil[$key];
    }

    function getEstatusAlumno($key){
        $estatus = estatusAlumno();
        return $estatus[$key];
    }
    function getUltimoId($table)
    {
        $id = DB::table($table)->latest()->value('id');
        return $id;
    }

    function getNroExpediente($table)
    {
        $ultId = getUltimoId($table) + 1;
        return config('application.expediente').'-'.$ultId;
    }


    function getFullNombre($table,$id)
    {
        $nombres = DB::table($table)->where('id', $id)->pluck('nombres')->implode(' ');
        $apellidos = DB::table($table)->where('id', $id)->pluck('apellidos')->implode(' ');
        return $nombres. ' ' .$apellidos;
    }

    function tiempoPermanencia($f_ing, $f_egre='') {
        $f_fin = (!empty($f_egre)) ? $f_egre : 'today';

        $f_desde = new DateTime($f_ing);
        $f_hasta = new DateTime($f_fin);
        $dateDiff = $f_desde->diff($f_hasta);
        $years = $dateDiff->y;
        $months = $dateDiff->m;
        $days = $dateDiff->d;
        $permanencia = "";
        if ($years != 0) {
            $string = ($years > 1) ? " años " : " año ";
            $permanencia .= $years . $string;
        }

        if ($months != 0) {
            $string = ($months > 1) ? " meses " : " mes ";
            $permanencia .= $months . $string;
        }

        if ($days != 0) {
            $string = ($days > 1) ? " dias " : " dia ";
            $permanencia .= $days . $string;
        }


        return $permanencia;
    }