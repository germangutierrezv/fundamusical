<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alumno extends Model
{

    protected $table='alumnos';
	protected $primaryKey = 'id';

	protected $fillable = array(
        		'cedula','nombres','apellidos','sexo',
				'f_nacimiento','email','estatus','telefono',
				'estado_civil','direccion','estado','estudia',
				'l_estudia','nivel'
    );

    protected $hidden = ['created_at','update_at'];

    public function persona()
    {
    	return $this->hasMany(Persona::class);
    }
}
