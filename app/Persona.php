<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
	protected $table='personas';

    public function alumno()
    {
    	return $this->hasMany(Alumno::class);
    }
}
