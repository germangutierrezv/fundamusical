<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Persona;

class PersonaController extends Controller
{
    public function index($tipo)
    {

    	switch ($tipo) {
    		case 'profesores':
    			$tipersona = 1;
    			break;
    		case 'representantes':
    			$tipersona = 0;
    			break;
    	}

    	$personas = Persona::where('isprofesor', $tipersona)
    			->orderBy('id', 'desc')
    			->paginate(config('application.elem_por_pag'));

    	return view('personas.index',['personas'=>$personas,'tipo'=>$tipo,]);
    }

    public function create()
    {
    	return view('personas.create');
    }
}
