<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AlumnoRequest;
use App\Http\Requests;
use App\Alumno;
use Auth;

class AlumnoController extends Controller
{

	protected $fields = [
		'expediente' => '',
		'nombres' => '',
		'apellidos' => '',
		'sexo' => '',
		'cedula' => '',
		'foto' => 'no_foto.png',
		'f_nacimiento' => '',
		'email' => '',
		'estatus' => '',
		'estado_civil' => '',
		'f_ingreso' => '',
		'direccion' => '',
		'estado' => 'A',
		'telefono' => '',
		'estudia' => '',
		'l_estudia' => '',
		'nivel' => '',
		'user_id' => '',
	];

	public function index(Request $request)
	{
		$alumnos = Alumno::where('nombres','like','%'.$request->input('buscar').'%')
			->orWhere('apellidos','like','%'.$request->input('buscar').'%')
			->orderBy('id', 'desc')->paginate(config('application.elem_por_pag'));
		
		return view('alumnos.index')->with('alumnos',$alumnos);
	}

	public function show($id)
	{
		$alumno = Alumno::findOrFail($id);
		return view('alumnos.show')->with('alumno',$alumno);
	}

    public function create()
    {
    	return view('alumnos.create');
    }


	public function store(AlumnoRequest $request)
	{

		$alumno = new Alumno();
		$alumno->expediente = getNroExpediente('alumnos');
		$alumno->cedula = $request->input('cedula');
		$alumno->nombres = $request->input('nombres');
		$alumno->apellidos = $request->input('apellidos');
		$alumno->sexo = $request->input('sexo');
		$alumno->f_nacimiento = $request->input('f_nacimiento');
		$alumno->email = $request->input('email');
		$alumno->estatus = $request->input('estatus');
		$alumno->telefono = $request->input('telefono');
		$alumno->estado_civil = $request->input('estado_civil');
		$alumno->direccion = $request->input('direccion');
		$alumno->estado = $request->input('estado');
		$alumno->estudia = $request->input('estudia');
		$alumno->l_estudia = $request->input('l_estudia');
		$alumno->nivel = $request->input('nivel');
		$alumno->user_id = Auth::user()->id;
		$alumno->f_ingreso = date('Y-m-d');

    	$alumno->save();

    	//return view('alumnos.show')->with(['alumno'=>$alumno,'success-message'=>'Alumno '.getFullNombre('alumnos',$alumno->id).' insertado correctamente!.']);
    	//return redirect()->to('/alumnos')->with('success-message','Alumno'.getFullNombre($alumno->id).' insertado correctamente!.');
		return redirect()->route('alumnos.show',$alumno->id)->with('success-message','Alumno ' . getFullNombre('alumnos',$alumno->id).'
			insertado correctamente.');

	}

	public function edit($id)
	{
		$alumno = Alumno::findOrFail($id);

		return view('alumnos.edit',['alumno'=>$alumno]);
	}

	public function update(Request $request, $id)
	{
		$this->validate($request, [
           	'nombres' => 'required|string|min:3',
            'apellidos' => 'required|string|min:3',
            'f_nacimiento' => 'required',
            'email'=>'required|email|unique:alumnos,email,'.$id,
            'sexo' => 'required',
            'direccion' => 'required',

        ]);

		$alumno = Alumno::findOrFail($id);

		$input = $request->only(['cedula','nombres','apellidos','sexo',
								'f_nacimiento','email','estatus','telefono',
								'estado_civil','direccion','estado','estudia',
								'l_estudia','nivel']);

		$alumno->fill($input)->save();

		return redirect()->route('alumnos.edit', $id)->with('success-message','Datos del Alumno '. getFullNombre('alumnos',$alumno->id) .' actualizados!.');		
	}

	public function changeFingreso(Request $request)
	{
		$alumno = Alumno::find($request->id);
		$alumno->f_ingreso = ($request->f_ingreso);
		$alumno->save();

		return redirect()->route('alumnos.show',$alumno->id)->with('success-message','Actualizada fecha de ingreso!.');
	}


}
