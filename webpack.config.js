const path = require('path');
const { VueLoaderPlugin } = require('vue-loader')

module.exports = {
	entry: './src/app/app.js',
	output: {
		path: path.join(__dirname, 'src/public/js'),
		filename: 'bundle.js'
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				loader: 'babel-loader',
				exclude: /node_modules/,
			},
			{
				test: /\.vue$/,
				exclude: /node_modules/,
				use: {
					loader: 'vue-loader'
				}
			},
			{
			  test: /\.css/,
			  use: ['vue-style-loader', 'css-loader'] // BOTH are needed!
			}
		]
	},
	resolve: { 
		extensions: ['.js', '.vue', '.json'],
		alias: 
		{ 
			'vue$': 'vue/dist/vue.esm.js',
		} 
	},
	plugins: [
		new VueLoaderPlugin()
	]
}