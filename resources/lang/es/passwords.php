<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'reset' => 'La contraseña ha sido reseteada.',
    'sent' => 'Se ha enviado un correo con el link para resetar la contraseña!',
    'token' => 'Este token para resetear la contraseña es inválido.',
    'user' => "No existe usuario registrado con ese correo.",

];
