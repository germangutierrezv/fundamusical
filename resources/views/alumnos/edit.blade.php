@extends('layouts.app')

@section('titulo')
{{ config('application.nombre')}} | Editar Alumno {{ $alumno->id}}
@endsection

@section('content')
<div class="panel panel-default">
		<div class="panel-heading">
			<div class="panel-title">
				<i class="fa fa-edit"></i> Editar Alumno
			</div>
		</div>
		<div class="panel-body">
			@include('layouts.partials.error')
			@include('layouts.partials.message')
			
			<form action="{{ route('alumnos.update', $alumno->id) }}" method="POST" class="form-horizontal">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="_method" value="PUT">
				
				<div class="form-group">
					<label for="expediente" class="col-md-3 control-label">
						Expediente
					</label>	
						<div class="col-md-2">
							<input type="text" class="form-control" name="expediente" id="expediente" pattern="^([VEJPG]{1})([0-9]{7,9})$" value="{{ $alumno->expediente }}" placeholder="V12345678" readonly>
						</div>


					<label for="cedula" class="col-md-3 control-label">
						Cédula
					</label>	
						<div class="col-md-3">
							<input type="text" class="form-control" name="cedula" id="cedula" value="{{ $alumno->cedula }}" placeholder="Ejemplo V12345778">
						</div>
				</div>
				<div class="form-group">
					<label for="nombres" class="col-md-3 control-label">
						Nombres
					</label>	
						<div class="col-md-8">
							<input type="text" class="form-control" name="nombres" id="nombres" value="{{ $alumno->nombres }}" required>
						</div>
				</div>
				<div class="form-group">
					<label for="apellidos" class="col-md-3 control-label">
						Apellidos
					</label>	
						<div class="col-md-8">
							<input type="text" class="form-control" name="apellidos" id="nombre" value="{{ $alumno->apellidos }}" required>
						</div>
				</div>

				<div class="form-group">
					<label for="reverse_direction" class="col-md-3 control-label">
						Sexo
					</label>	
						<div class="col-md-4">
							<label class="radio-inline">
								<input type="radio" name="sexo" id="sexo" 
								@if ($alumno->sexo=='M')
									checked="checked"
								@endif
								value="M"> Masculino	
							</label>
							<label class="radio-inline">
								<input type="radio" name="sexo" 
								@if ($alumno->sexo == 'F')
									checked="checked"
								@endif
								value="F"> Femenino	
							</label>
							<label class="radio-inline">
								<input type="radio" name="sexo" 
								@if ($alumno->sexo == 'O')
									checked="checked"
								@endif
								value="O"> Otro	
							</label>	
						</div>
					<label for="estatus" class="col-md-1 control-label">
						Estatus
					</label>	
						<div class="col-md-2">
							<select name="estatus" class="form-control">
								<option value="{{ $alumno->estatus }}">{{ getEstatusAlumno($alumno->estatus) }}</option>
								@foreach( estatusAlumno() as $estatus => $status)
									<option  value="{{ $estatus }}">{{ $status }}</option>
								@endforeach
							</select>
						</div>

				</div>
				<div class="form-group">
					<label for="email" class="col-md-3 control-label">
						Correo Electrónico
					</label>	
						<div class="col-md-3">
							<input type="email" class="form-control" name="email" id="email" value="{{ $alumno->email }}" required placeholder="prueba@ejemplo.com">
						</div>
					<label for="telefono" class="col-md-2 control-label">
						Telefóno
					</label>	
					<div class="col-md-3">
						<div class="input-group">
							<div class="input-group-addon">
                    			<i class="fa fa-phone"></i>
                  			</div>
							<input type="tel"  pattern="[0-9]{4}-?[0-9]{7}" class="form-control" name="telefono" id="telefono" data-inputmask='"mask": "9999-9999999"' data-mask value="{{ $alumno->telefono }}" placeholder="0416-1234567">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="email" class="col-md-3 control-label">
						Fecha de Nacimiento
					</label>	
						<div class="col-md-2">
							<div class="input-group">
								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
									<input class="form-control input-sm" type="date" name="f_nacimiento" id="f_nacimiento" value="{{ getFormatDate($alumno->f_nacimiento) }}" max="<?php echo date('Y-m-d'); ?>" step="1" required autocomplete="on" >
							</div>
						</div>

					<label for="estado_civil" class="col-md-3 control-label">
						Estado Civil
					</label>	
						<div class="col-md-3">
							<select name="estado_civil" class="form-control">
									<option value="{{ $alumno->estado_civil }}">{{ getEstadoCivil($alumno->estado_civil) }}</option>
								@foreach( estadoCivil() as $estado => $stat)
									<option  value="{{ $estado }}">{{ $stat }}</option>
								@endforeach
							</select>
						</div>
				</div>
				<div class="form-group">
					<label for="direccion" class="col-md-3 control-label">
						Dirección
					</label>	
						<div class="col-md-4">
							<textarea class="form-control" name="direccion" id="direccion" rows="2">{{ $alumno->direccion }}
							</textarea>
						</div>
					<label for="estado" class="col-md-1 control-label">
						Estado
					</label>	
						<div class="col-md-3">
							<select name="estado" class="form-control">
								<option value="{{ $alumno->estado }}">{{ getEstadoPais($alumno->estado) }}</option>
								@foreach( estadoPais() as $estado => $stado)
									<option  value="{{ $estado }}">{{ $stado }}</option>
								@endforeach
							</select>
						</div>

				</div>
				<hr>
				<div class="form-group">
					<label for="reverse_direction" class="col-md-3 control-label">
						Estudia?
					</label>	
						<div class="col-md-2">
							<label class="radio-inline">
								<input type="radio" name="estudia" id="estudia" 
								@if ( $alumno->estudia )
									checked="checked"
								@endif
								value="1"> Si	
							</label>
							<label class="radio-inline">
								<input type="radio" name="estudia" id="estudia" 
								@if ( ! $alumno->estudia )
									checked="checked"
								@endif
								value="0"> No	
							</label>	
						</div>
					<label for="reverse_direction" class="col-md-3 control-label">
						Nivel
					</label>
					<div class="col-md-3">
						<input type="text" class="form-control" name="nivel" id="nivel" value="{{ $alumno->nivel }}" placeholder="Ejemplo 6to Grado">
					</div>

				</div>
				<div class="form-group">
					<label for="direccion" class="col-md-3 control-label">
						Instituto Estudia
					</label>	
						<div class="col-md-8">
							<input type="text" class="form-control" name="l_estudia" id="l_estudia" value="{{ $alumno->l_estudia }}" placeholder="Ejemplo Liceo de Ejido">
						</div>
				</div>


				<div class="form-group">
					<div class="col-md-7 col-md-offset-3">
						<button type="submit" class="btn btn-success btn-md"><i class="fa fa-save"></i>&nbsp; Guardar</button>
						<a href="{{ route('alumnos.show',$alumno->id) }}" class="btn btn-primary"><i class="fa fa-chevron-left"></i> Volver</a>
					</div>
				</div>
			
			</form>
		</div>
	</div>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){
		var estatus = $("input[name='estatus']").val();
		var estado_civil = $("input[name='estado_civil']").val();
		var estado = $("input[name='estado']").val();
		
	

		alert(estatusAlumno());
	});
</script>
@endsection