@extends('layouts.app')

@section('titulo')
{{ config('application.nombre')}} | Alumnos
@endsection

@section('content')

<div class="panel panel-default">
	<div class="panel-heading">
		<h1 class="text-primary text-right">
			<i class="fa fa-users"></i> Alumnos
			<a href="{{ route('alumnos.create') }}" class="btn btn-success btn-sm pull-left"><i class="fa fa-plus"></i> Nuevo Alumno</a>
		</h1>
	</div>
	<div class="panel-body">
		@include('layouts.partials.error')
		@include('layouts.partials.message')

		@if ($alumnos->count())
			<div class="search">
				<form action="{{ route('alumnos.index')}}" method="GET" class="form-horizontal">
					<div class="form-group">
						<div class="input-group col-sm-offset-1 col-sm-10">
							<input type="text" name="buscar" class="form-control" value="{{ request()->buscar }}" placeholder="Buscar Alumnos..">
							<span class="input-group-btn">
								<button type="submit" class="btn btn-default"><i class="fa fa-btn fa-search"></i> Buscar</button>
							</span>
						</div>
					</div>
				</form>
			</div>
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th># Expediente</th>
						<th>Cédula</th>
						<th>Nombres</th>
						<th>Apellidos</th>
						<th>Estatus</th>
					</tr>
				</thead>	
				<tbody>
					@foreach ($alumnos as $alumno)
					<tr class="">
						<td><a href="{{ route('alumnos.show',$alumno->id)}}">{{ $alumno->expediente }}</td></a>
						<td>{{ $alumno->cedula }}</td>
						<td>{{ $alumno->nombres }}</td>
						<td>{{ $alumno->apellidos }}</td>
						<td><span class="badge">{{ getEstatus($alumno->estatus) }}</span></td>
					</tr>
					@endforeach
				</tbody>
			</table>
			<div class="text-center">
				{{ $alumnos->links() }}
			</div>
		@else
			<h3>No hay Alumnos</h3>
		@endif

	</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
		
</script>
@endsection