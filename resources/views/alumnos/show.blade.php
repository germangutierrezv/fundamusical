@extends('layouts.app')

@section('titulo')
{{ config('application.nombre')}} | Alumno {{ $alumno->id}}
@endsection

@section('content')
            @include('layouts.partials.message')

	<div class="panel panel-{{($alumno->estatus =='A')?'primary':'danger'}}">
		<div class="group-btn pull-right">
			<a href="{{ route('alumnos.create') }}" class="btn btn-warning btn-sm" ><i class="fa fa-plus"></i> Nuevo Alumno</a>
		</div>

		<div class="panel-heading">
			<h3 class="panel-title">
				Expediente {{ $alumno->expediente}} - {{ getFullNombre('alumnos',$alumno->id)}}
			</h3>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-sm-2" align="center">
                    <div class="form-group">
					   <img src="{{ getImage('alumnos',$alumno->id) }}" class="img-circle">
                    </div>
                    <div class="form-group" align="center">
                        <form action="/alumnos" class="form" role="form" enctype="multipart/form-data" method="POST">
                            <label class="btn btn-default btn-xs btn-file image-input">
                                Cambiar <input type="file" accept="image/png, image/jpeg, image/bmp" name="foto">
                            </label>
                        </form>
                    </div>
				</div>

				<div class="col-sm-6">
					
				</div>
				<div class="col-sm-4">
					<ul class="list-group">
						<li class="list-group-item">Fecha de Ingreso
							<span class="badge">{{ getFormatFecha($alumno->f_ingreso) }}</span>
                            <span class="badge"><a href="" title="Editar fecha de Ingreso" data-toggle="modal" data-target="#f_ingresoModal" class="btn-edit-fing"><i class="fa fa-edit"></i></a></span>
						</li>
						<li class="list-group-item">Tiempo 
							<span class="badge">{{ tiempoPermanencia($alumno->f_ingreso) }}</span>
						</li>
						<li class="list-group-item">Fecha Nacimiento
							<span class="badge">{{ getFormatFecha($alumno->f_nacimiento) }}</span>
						</li>
						<li class="list-group-item">Edad
							<span class="badge">{{ getEdad($alumno->f_nacimiento) }}</span>
						</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#tab-basicos" aria-controls="tab-basicos" role="tab" data-toggle="tab">Datos Básicos</a></li>
                    <li role="presentation" ><a href="#tab-educativos" aria-controls="tab-educativos" role="tab" data-toggle="tab">Datos Educativos</a></li>
				</ul>
			</div>
			<div class="tab-content" style="margin-top: 10px;">
				<div role="tabpanel" class="tab-pane fade in active" id="tab-basicos">
                    <div class="col-sm-2">
                        <div class="form-group required">
                            <label class="control-label" for="nombre">Cédula:</label>
                            <input class="form-control input-sm" type="text" name="cedula" value="{{ $alumno->cedula }}" autocomplete="off" readonly />
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group required">
                            <label class="control-label" for="nombre">Nombres:</label>
                            <input class="form-control input-sm" type="text" name="nombres" value="{{ $alumno->nombres }}" autocomplete="off" readonly />
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group required">
                            <label class="control-label" for="nombre">Apellidos:</label>
                            <input class="form-control input-sm" type="text" name="apellidos" value="{{ $alumno->apellidos }}" autocomplete="off" readonly/>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group required">
                            <label class="control-label" for="nombre">Sexo:</label>
                            <input class="form-control input-sm" type="text" name="sexo" value="{{ getSexo($alumno->sexo) }}" autocomplete="off" readonly/>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group required">
                            <label class="control-label" for="nombre">Estado Civil:</label>
                            <input class="form-control input-sm" type="text" name="estatus" value="{{ getEstadoCivil($alumno->estado_civil ) }}" autocomplete="off" readonly/>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group required">
                            <label class="control-label" for="email">Correo Electrónico:</label>
                            <input class="form-control input-sm" type="text" name="estatus" value="{{ $alumno->email }}" autocomplete="off" readonly/>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group required">
                            <label class="control-label" for="telefono">Telefóno:</label>
                            <input class="form-control input-sm" data-inputmask=""mask":"(9999) 999-9999 "" data-mask="" type="text" name="telefono" value="{{ $alumno->telefono }}" autocomplete="off" readonly/>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group required">
                            <label class="control-label" for="f_nacimiento">Fecha Nacimiento:</label>
                            <input class="form-control input-sm" type="text" name="f_nacimiento" value="{{ getFormatFecha($alumno->f_nacimiento) }}" autocomplete="off" readonly/>
                        </div>
                    </div>
                    <div class="col-sm-2">
                       <div class="form-group required">
                            <label class="control-label" for="nombre">Estatus:</label>
                            <input class="form-control input-sm" type="text" name="estatus" value="{{ getEstatus($alumno->estatus) }}" autocomplete="off" readonly/>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group required">
                            <label class="control-label" for="direccion">Dirección:</label>
                            <textarea class="form-control input-sm" readonly>{{ $alumno->direccion }}</textarea>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group required">
                            <label class="control-label" for="email">Estado:</label>
                            <input class="form-control input-sm" type="text" name="estatus" value="{{ getEstadoPais($alumno->estado) }}" autocomplete="off" readonly/>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tab-educativos">
                    <div class="form-group">
                    <label for="estudia" class="col-md-2 control-label">
                        Estudia?
                    </label>    
                        <div class="col-md-1">
                            <label class="radio-inline">
                                <input type="radio" name="estudia" id="estudia" 
                                @if ( $alumno->estudia )
                                    checked="checked"
                                @endif
                                value="1"> Si   
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="estudia" id="estudia" 
                                @if ( ! $alumno->estudia )
                                    checked="checked"
                                @endif
                                value="0"> No   
                            </label>    
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group required">
                            <label class="control-label" for="nombre">Nivel:</label>
                            <input class="form-control input-sm" type="text" name="cedula" value="{{ $alumno->nivel }}" autocomplete="off" placeholder="Ejemplo 6to Grado" readonly />
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group required">
                            <label class="control-label" for="nombre">Institución donde Estudia:</label>
                            <input class="form-control input-sm" type="text" name="cedula" value="{{ $alumno->l_estudia }}" autocomplete="off" placeholder="Ejemplo Liceo de Ejido" readonly />
                        </div>
                    </div>
				</div>
			</div>
		</div>
        <!--Modal Fecha Ingreso -->
	    <div class="modal fade" id="f_ingresoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content modal-sm">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Fecha de Ingreso</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                     </div>

                    <div class="modal-body">
                        <form action="{{ route('alumnos.changeFingreso', $alumno->id) }}" method="POST" class="form-horizontal">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="_method" value="PUT">
                            <input type="hidden" name="id" value="{{ $alumno->id }}">

                            <div class="form-group">
                            <label for="f_nacimiento" class="col-md-3 control-label">
                                Ingreso
                            </label>
                            <div class="col-md-2">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </span>
                                        <input class="form-control input-sm" type="date" name="f_ingreso" id="f_ingreso" value="{{ getFormatDate($alumno->f_ingreso) }}" max="<?php echo date('Y-m-d'); ?>" step="1" required autocomplete="on" >
                                </div>
                            </div>
                            </div>
                    </div>
                        <div class="modal-footer">
                            <div class="col-md-offset-2 pull-right">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                <button type="submit" class="btn btn-success">Cambiar</button>
                            </div>
                        </div>    
                        </form>

                </div>
            </div>
        </div>   
        <div class="panel-footer">
            <div class="text-right">
    			<a href="{{ route('alumnos.edit',$alumno->id) }}" class="btn btn-danger"><i class="fa fa-edit"> Editar</i></a>
                <a href="" class="btn btn-info"><i class="fa fa-file"></i> Imprimir Ficha</a>
    			<a href="{{ route('alumnos.index') }}" class="btn btn-primary"><i class="fa fa-chevron-left"></i>Volver</a>
            </div>
		</div>
	</div>

@endsection