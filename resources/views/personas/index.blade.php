@extends('layouts.app')

@section('titulo')
{{ config('application.nombre')}} | {{ ucfirst($tipo) }}
@endsection

@section('content')
<div class="panel panel-default">
	<div class="panel-heading">
		<h1 class="text-primary text-right">
			<i class="fa fa-users"></i> {{ ucfirst($tipo) }} 
			<a href="{{ route('personas.create','tipo', $tipo) }}" class="btn btn-success btn-sm pull-left"><i class="fa fa-plus"></i> Nuevo {{ ucfirst($tipo) }}</a>
		</h1>
	</div>
	<div class="panel-body">
		@if ($personas)
		
		<div class="">
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Cédula</th>
						<th>Nombres</th>
						<th>Apellidos</th>
						<th>Condición</th>
					</tr>
				</thead>
				<tbody>
					@foreach($personas as $persona)
					<tr>
						<td><a href="{{ route('personas.show',['persona'=>$persona->id,'tipo'=>$tipo])}}">{{ $persona->cedula }}</a></td>
						<td>{{ $persona->nombres }}</td>
						<td>{{ $persona->apellidos }}</td>
						<td><span class="badge">{{ getEstatus($persona->estatus) }}</span></td>
					</tr>
					@endforeach
				</tbody>
			</table>
			<div class="text-center">
				{!! $personas->links() !!} 	
			</div>
		</div>

		 
		@else 
			No hay registrado {{ ucfirst($tipo)}}
		@endif

	</div>
</div>
@endsection