<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ config('application.nombre') }} | Iniciar Sesión</title>
 
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="description" content="FunMusica es un software de gestión de la Fundación Musical Nucleo Ejido. Creado por Ing. German Gutierrez (soporte@sercontec.com.ve)" />

  <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" />

  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
  <!-- Font Awesome-->
  <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
  <!--Ioicons-->
  <link href="{{ asset('css/ionicons.min.css') }}" rel="stylesheet">
  <!--Estilo de Tema -->
  <link href="{{ asset('css/AdminLTE.min.css') }}" rel="stylesheet">
  <!--iCheck -->
  <link href="{{ asset('css/blue.css') }}" rel="stylesheet">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="text-center">
    <img src="{{ asset('img/logo.png') }}" width="100px" height="100px" class="img-rounded">
  </div>
  <div class="login-logo">
    <a href="{{ url('/') }}"><b>{{ config('application.nombre')}}</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Iniciar Sesión</p>
    @if ($errors->has('email') or $errors->has('password'))
        <span class="help-block alert alert-danger">
          <strong>El usuario y/o contraseña no corresponde {{ $errors->first('email') }} {{ $errors->first('password') }}</strong>
        </span>
   @endif
  <form action="{{ route('login') }}" method="POST">
    {{ csrf_field() }}
      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
        <input  id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
        <span class="fa fa-envelope form-control-feedback"></span>
      </div>
      <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} has-feedback">
        <input id="password" type="password" class="form-control" name="password" required placeholder="Contraseña">
        <span class="fa fa-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox">
            <label>
              <!--<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recordar -->
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Ingresar</button>
        </div>
        <!-- /.col -->
      </div>
    </form>   
    <!--Ingreso coon otras opciones 
    <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
        Google+</a>
    </div>
    <!-- /.social-auth-links -->

    <a href="{{ route('password.request') }}">Olvidó Contraseña?</a><br>
   <!-- <a href="register.html" class="text-center">Register a new membership</a>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
<footer class="footer">

  @include('layouts.partials.footer')
</footer>

<script src="{{ asset('js/jquery.min.js') }}"></script>

<script src="{{ asset('js/bootstrap.min.js') }}"></script>

<script src="{{ asset('js/icheck.min.js') }}"></script>

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
</body>
</html>