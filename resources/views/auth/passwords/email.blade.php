
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ config('application.nombre') }} | Iniciar Sesión</title>
 
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="description" content="FunMusica es un software de gestión de la Fundación Musical Nucleo Ejido. Creado por Ing. German Gutierrez (soporte@sercontec.com.ve)" />

  <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" />

  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
  <!-- Font Awesome-->
  <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
  <!--Ioicons-->
  <link href="{{ asset('css/ionicons.min.css') }}" rel="stylesheet">
  <!--Estilo de Tema -->
  <link href="{{ asset('css/AdminLTE.min.css') }}" rel="stylesheet">
  <!--iCheck -->
  <link href="{{ asset('css/blue.css') }}" rel="stylesheet">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="login-page">

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="login">
                          <div class="text-center">
                            <img src="{{ asset('img/logo.png') }}" width="100px" height="100px" class="img-rounded">
                          </div>
                          <div class="login-logo">
                            <a href="{{ url('/') }}"><b>{{ config('application.nombre')}}</b></a>
                          </div>
                        </div>
                </div>

                    <div class="panel-body">
                        <div class="text-primary text-center">
                            <h2><i class="fa fa-key"></i> Resetear Contraseña</h2>    
                        </div>
                        <br>
                        <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">Correo Electrónico</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Resetear
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div class="panel-footer">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>


<footer class="footer text-center">
  @include('layouts.partials.footer')
</footer>

<script src="{{ asset('js/jquery.min.js') }}"></script>

<script src="{{ asset('js/bootstrap.min.js') }}"></script>

<script src="{{ asset('js/icheck.min.js') }}"></script>

</body>
</html>



