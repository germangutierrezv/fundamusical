@if (Session::has("success-message"))
	<div class="alert alert-success alert-block">
	        <button type="button" class="close" data-dismiss="alert">&times;</button>
	            <strong>{{ Session::get("success-message") }}</strong>
    </div>
@elseif (Session::has("error-message"))
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>{{ Session::get("error-message") }}</strong>
    </div>
@endif